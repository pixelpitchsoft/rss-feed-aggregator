<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Normalizer;

use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Description of Uuid
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class UuidNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = []): string
    {
        /* @var $object Uuid */
        return $object->toString();
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Uuid;
    }
}
