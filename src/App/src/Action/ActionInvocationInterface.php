<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
interface ActionInvocationInterface
{
    public function indexAction(ServerRequestInterface $request): ResponseInterface;
}
