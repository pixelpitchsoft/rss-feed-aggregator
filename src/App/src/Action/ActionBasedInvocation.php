<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Action;

use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * Description of ActionBasedInvocation
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
trait ActionBasedInvocation
{

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $action = $request->getAttribute('action', 'index') . 'Action';

        if (! method_exists($this, $action)) {
            return new EmptyResponse(StatusCode::STATUS_NOT_FOUND);
        }

        return $this->$action($request);
    }
}
