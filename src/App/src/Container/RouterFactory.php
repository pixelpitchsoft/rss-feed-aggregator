<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Container;

/**
 * Description of RouterFactory
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class RouterFactory
{
    /**
     * @param ContainerInterface $container
     * @return FastRouteBridge
     */
    public function __invoke(ContainerInterface $container)
    {
        return new FastRouteBridge(
            $container->get(FastRoute\RouteCollector::class),
            $container->get(FastRoute\DispatcherFactory::class)
        );
    }
}
