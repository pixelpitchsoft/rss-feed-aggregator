<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Container;

use FastRoute\Dispatcher\GroupPosBased as FastRouteDispatcher;
use Psr\Container\ContainerInterface;

/**
 * Description of FastRouteDispatcherFactory
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class FastRouteDispatcherFactory
{
    /**
     * @param ContainerInterface $container
     * @return callable
     */
    public function __invoke(ContainerInterface $container)
    {
        return function ($data) {
            return new FastRouteDispatcher($data);
        };
    }
}
