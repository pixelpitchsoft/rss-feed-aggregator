<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Factory\Command;

use Psr\Container\ContainerInterface;
use App\Command\FixtureSeedCommand;
use Nelmio\Alice\Loader\NativeLoader;

/**
 * Description of FixtureSeedCommandFactory
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class FixtureSeedCommandFactory
{
    /**
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     *
     * @var FixtureSeedCommand
     */
    private $fixtureSeedCommand;

    public function __invoke(ContainerInterface $container)
    {
        $this->setContainer($container);
        $this->setUpFixtureSeedCommand();

        return $this->getFixtureSeedCommand();
    }

    private function setUpFixtureSeedCommand()
    {
        $container = $this->getContainer();
        $fixtureSeedCommand = new FixtureSeedCommand(
            $container->get('config')['fixtures.path'],
            $container->get('doctrine.entity_manager.orm_default'),
            new NativeLoader(),
            $container->get('config')['progress.bar.percentage'],
            $container->get('config')['progress.initial.stage.static.advance']
        );

        $this->setFixtureSeedCommand($fixtureSeedCommand);
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function getFixtureSeedCommand(): FixtureSeedCommand
    {
        return $this->fixtureSeedCommand;
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }

    public function setFixtureSeedCommand(FixtureSeedCommand $fixtureSeedCommand)
    {
        $this->fixtureSeedCommand = $fixtureSeedCommand;
        return $this;
    }
}
