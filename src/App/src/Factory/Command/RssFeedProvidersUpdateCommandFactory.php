<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Factory\Command;

use Psr\Container\ContainerInterface;
use App\Command\RssFeedProvidersUpdateCommand;
use Provider\Model\LoadDataModel;

/**
 * Description of RssFeedProvidersUpdateCommandFactory
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class RssFeedProvidersUpdateCommandFactory
{
    /**
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     *
     * @var RssFeedProvidersUpdateCommand
     */
    private $rssFeedProvidersUpdateCommand;

    public function __invoke(ContainerInterface $container)
    {
        $this->setContainer($container);
        $this->setUpRssFeedProvidersUpdateCommand();

        return $this->getRssFeedProvidersUpdateCommand();
    }

    private function setUpRssFeedProvidersUpdateCommand()
    {
        $container = $this->getContainer();
        $fixtureSeedCommand = new RssFeedProvidersUpdateCommand(
            $container->get('config')['progress.bar.percentage'],
            $container->get('config')['progress.initial.stage.static.advance'],
            $container->get(LoadDataModel::class)
        );

        $this->setRssFeedProvidersUpdateCommand($fixtureSeedCommand);
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function getRssFeedProvidersUpdateCommand(): RssFeedProvidersUpdateCommand
    {
        return $this->rssFeedProvidersUpdateCommand;
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }

    public function setRssFeedProvidersUpdateCommand(RssFeedProvidersUpdateCommand $rssFeedProvidersUpdateCommand)
    {
        $this->rssFeedProvidersUpdateCommand = $rssFeedProvidersUpdateCommand;
        return $this;
    }
}
