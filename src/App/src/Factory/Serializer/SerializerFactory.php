<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Factory\Serializer;

use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Normalizer\UuidNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

/**
 * Description of SerializerFactory
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class SerializerFactory
{
    /**
     *
     * @var Serializer
     *
     */
    private $serializer;

    /**
     *
     * @var ContainerInterface
     */
    private $container;

    public function __invoke(ContainerInterface $container)
    {
        $this->setContainer($container);
        $this->setUpSerializer();

        return $this->getSerializer();
    }

    private function setUpSerializer()
    {
        $encoders    = [];
        $container = $this->getContainer();
        switch ($container->get('config')['api.format']) {
            case \App\Aggregator\API_FORMAT_JSON:
                $encoders[] = new JsonEncoder();
                break;
            default:
                $encoders[] = new XmlEncoder();
                break;
        }
        $normalizers = [
            new UuidNormalizer(),
            new DateTimeNormalizer(),
            new ObjectNormalizer(),
        ];

        $serializer = new Serializer($normalizers, $encoders);

        $this->setSerializer($serializer);
    }

    public function getSerializer(): Serializer
    {
        return $this->serializer;
    }

    public function setSerializer(Serializer $serializer)
    {
        $this->serializer = $serializer;
        return $this;
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }
}
