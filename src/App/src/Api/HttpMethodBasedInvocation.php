<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Api;

use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * Description of ActionBasedInvocation
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
trait HttpMethodBasedInvocation
{

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        if (!in_array($request->getMethod(), $this->getAllowedMethods())) {
            return EmptyResponse(StatusCode::STATUS_METHOD_NOT_ALLOWED);
        }
        switch ($request->getMethod()) {
            case \App\Aggregator\METHOD_GET:
            case \App\Aggregator\METHOD_HEAD:
                if (null !== $request->getAttribute('id')) {
                    return $this->display($request);
                }

                $pages  = [
                    isset($request->getQueryParams()['pageFrom']) ? $request->getQueryParams()['pageFrom'] : 1,
                    isset($request->getQueryParams()['pageTo']) ? $request->getQueryParams()['pageTo'] : 2,
                ];

                $this->setPages($pages);
                $limits = [
                    ($pages[0] - 1) * \App\Aggregator\ITEMS_PER_PAGE,
                    ($pages[1] - $pages[0]) * \App\Aggregator\ITEMS_PER_PAGE
                ];
                $this->setLimits($limits);

                return $this->displayList($request);
            case \App\Aggregator\METHOD_POST:
                if (null !== $request->getAttribute('id')) {
                    return EmptyResponse(StatusCode::STATUS_NOT_FOUND);
                }

                if (null === $request->getParsedBody()) {
                    return EmptyResponse(StatusCode::STATUS_EXPECTATION_FAILED);
                }

                return $this->add($request);
            case \App\Aggregator\METHOD_PUT:
                if (null === $request->getAttribute('id')) {
                    return EmptyResponse(StatusCode::STATUS_NOT_ACCEPTABLE);
                }
                if (in_array(\App\Aggregator\METHOD_PUT, $this->getAllowedMethods())) {
                    return $this->update($request);
                }
                throw new MethodNotAllowedException($request, $this->getAllowedMethods());
            case \App\Aggregator\METHOD_DELETE:
                if (null === $request->getAttribute('id')) {
                    return EmptyResponse(StatusCode::STATUS_NOT_ACCEPTABLE);
                }
                if (in_array(\App\Aggregator\METHOD_DELETE, $this->getAllowedMethods())) {
                    return $this->delete($request);
                }
                return EmptyResponse(StatusCode::STATUS_METHOD_NOT_ALLOWED);
            default:
                return new EmptyResponse(StatusCode::STATUS_NOT_FOUND);
        }
    }
}
