<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Api;

use Doctrine\ORM\EntityManager;
use App\RestUtil\RestActionInterface;
use Symfony\Component\Serializer\Serializer;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Description of AbstractApi
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
abstract class AbstractApi implements RestActionInterface
{
    /**
     *
     * @var EntityManager
     */
    private $entityManager;

    /**
     *
     * @var Serializer
     */
    private $serializer;

    /**
     *
     * @var array
     */
    private $pages;

    /**
     *
     * @var array
     */
    private $limits;

    /**
     *
     * @var array
     */
    private $addedHeaders;

    /**
     *
     * @var string
     */
    private $serializationFormat;

    /**
     *
     * @var array
     */
    private $allowedMethods;

    /**
     *
     * @var int
     */
    private $itemsPerPage;

    /**
     *
     * @var string
     */
    private $streamDirectory;

    /**
     *
     * @var string
     */
    private $streamMode;

    abstract public function delete(ServerRequestInterface $request): ResponseInterface;

    abstract public function display(ServerRequestInterface $request): ResponseInterface;

    abstract public function displayList(ServerRequestInterface $request): ResponseInterface;

    abstract public function add(ServerRequestInterface $request): ResponseInterface;

    abstract public function update(ServerRequestInterface $request): ResponseInterface;

    public function __construct(
        EntityManager $entityManager,
        Serializer $serializer,
        array $addedHeaders,
        string $serializationFormat,
        array $allowedMethods,
        int $itemsPerPage,
        string $streamDirectory,
        string $streamMode
    ) {
        $this->entityManager = $entityManager;
        $this->serializer    = $serializer;
        $this->addedHeaders  = $addedHeaders;
        $this->serializationFormat = $serializationFormat;
        $this->allowedMethods  = $allowedMethods;
        $this->itemsPerPage    = $itemsPerPage;
        $this->streamDirectory = $streamDirectory;
        $this->streamMode      = $streamMode;
    }

    protected function formulateResponseAddedHeaders(ResponseInterface $response): ResponseInterface
    {
        $addedHeaders = $this->getAddedHeaders();

        foreach ($addedHeaders as $name => $header) {
            $response = $response->withAddedHeader($name, $header);
        }

        return $response;
    }


    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    public function getSerializer(): Serializer
    {
        return $this->serializer;
    }

    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    public function setSerializer(Serializer $serializer)
    {
        $this->serializer = $serializer;
        return $this;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function getLimits()
    {
        return $this->limits;
    }

    public function setPages($pages)
    {
        $this->pages = $pages;
        return $this;
    }

    public function setLimits($limits)
    {
        $this->limits = $limits;
        return $this;
    }

    public function getAddedHeaders()
    {
        return $this->addedHeaders;
    }

    public function setAddedHeaders($addedHeaders)
    {
        $this->addedHeaders = $addedHeaders;
        return $this;
    }

    public function getSerializationFormat()
    {
        return $this->serializationFormat;
    }

    public function setSerializationFormat($serializationFormat)
    {
        $this->serializationFormat = $serializationFormat;
        return $this;
    }

    public function getAllowedMethods()
    {
        return $this->allowedMethods;
    }

    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    public function setAllowedMethods($allowedMethods)
    {
        $this->allowedMethods = $allowedMethods;
        return $this;
    }

    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
        return $this;
    }

    public function getStreamDirectory()
    {
        return $this->streamDirectory;
    }

    public function setStreamDirectory($streamDirectory)
    {
        $this->streamDirectory = $streamDirectory;
        return $this;
    }

    public function getStreamMode()
    {
        return $this->streamMode;
    }

    public function setStreamMode($streamMode)
    {
        $this->streamMode = $streamMode;
        return $this;
    }
}
