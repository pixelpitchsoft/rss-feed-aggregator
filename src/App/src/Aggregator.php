<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Aggregator;

/**
 * Application name
 */
const APPLICATION_NAME = 'RSS Feed Aggregator';
const APPLICATION_VERSION = '1.0.0.17';

const ITEMS_PER_PAGE = 20;
const API_FORMAT_JSON = 'json';
const API_FORMAT_XML = 'xml';

/**
 * API format
 */
const API_FORMAT = \App\Aggregator\API_FORMAT_JSON;

/**
 * API headers
 */
const API_HEADERS = [
    'Content-type' => 'application/' . \App\Aggregator\API_FORMAT,
    'Accept' => 'application/' . \App\Aggregator\API_FORMAT,
];

/**
 * API segmentation
 */
const API_VERSION = \App\Aggregator\APPLICATION_VERSION;
const API_LINGUISTIC_SEO_LABEL = 'api';
const API_PREFIX = '/'
        . \App\Aggregator\API_LINGUISTIC_SEO_LABEL
        . '/v'
        . \App\Aggregator\API_VERSION;

/**
 * API features
 */
const API_ROUTE_ID_REGEX = '\w{8}\-\w{4}\-\w{4}\-\w{4}-\w{12}';

/**
 * API formation
 */
const METHOD_POST = 'POST';
const METHOD_GET = 'GET';
const METHOD_HEAD = 'HEAD';
const METHOD_PUT = 'PUT';
const METHOD_DELETE = 'DELETE';

const API_ALLOWED_METHODS = [
    \App\Aggregator\METHOD_GET,
    \App\Aggregator\METHOD_HEAD,
    \App\Aggregator\METHOD_POST,
    \App\Aggregator\METHOD_DELETE,
    \App\Aggregator\METHOD_PUT,
];

/**
 * Progress bar information
 */
const PROGRESS_BAR_PERCENTAGE = 100;
const PROGRESS_INITIAL_STATIC_STAGE_ADVANCE = 20;
