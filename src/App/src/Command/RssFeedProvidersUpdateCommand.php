<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Command\SymfonyStyledProgress;
use Provider\Model\LoadDataModel;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Description of RssFeedProvidersUpdateCommand
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class RssFeedProvidersUpdateCommand extends Command
{

    use SymfonyStyledProgress;

    /**
     *
     * @var LoadDataModel
     */
    private $loadDataModel;

    public function __construct(
        int $progressBarPercentage,
        int $progressInitialAdvantage,
        LoadDataModel $loadDataModel
    ) {
        parent::__construct();
        $this->progressBarPercentage    = $progressBarPercentage;
        $this->progressInitialAdvantage = $progressInitialAdvantage;
        $this->loadDataModel            = $loadDataModel;
    }

    protected function configure()
    {
        $this
            ->setName('app:rss-providers-update')
            ->setDescription('Updates database stored RSS feed providers')
            ->setHelp('This command allows you to retrieve updated information'
                    . ' about RSS feed providers which you could youse in the'
                    . ' website RSS feed providers page');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Data Load');
        $io->section('Loading data');
        $io->createProgressBar($this->getProgressBarPercentage());
        $this->loadDataModel->setIo($io);
        try {
            $this->loadDataModel->loadData($this->getProgressBarPercentage());
            $io->progressFinish();
            $io->success('Data loaded');
        } catch (\Exception $e) {
            $io->progressFinish();
            $io->error($e->getMessage());
        }
    }

    public function getLoadDataModel(): LoadDataModel
    {
        return $this->loadDataModel;
    }

    public function setLoadDataModel(LoadDataModel $loadDataModel)
    {
        $this->loadDataModel = $loadDataModel;
        return $this;
    }
}
