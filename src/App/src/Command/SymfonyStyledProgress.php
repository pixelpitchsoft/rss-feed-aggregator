<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Command;

use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Description of SymfonyStyledProgress
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
trait SymfonyStyledProgress
{
    /**
     *
     * @var int
     */
    private $progressBarPercentage;

    /**
     *
     * @var int
     */
    private $progressInitialAdvantage;

    public function getProgressBarPercentage()
    {
        return $this->progressBarPercentage;
    }

    public function getProgressInitialAdvantage()
    {
        return $this->progressInitialAdvantage;
    }

    public function setProgressBarPercentage($progressBarPercentage)
    {
        $this->progressBarPercentage = $progressBarPercentage;
        return $this;
    }

    public function setProgressInitialAdvantage($progressInitialAdvantage)
    {
        $this->progressInitialAdvantage = $progressInitialAdvantage;
        return $this;
    }
}
