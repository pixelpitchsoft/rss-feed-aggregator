<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nelmio\Alice\Loader\NativeLoader;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Command\SymfonyStyledProgress;

/**
 * Description of Fixtures
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class FixtureSeedCommand extends Command
{

    use SymfonyStyledProgress;

    /**
     *
     * @var string
     */
    private $fixturesPath;

    /**
     *
     * @var EntityManager
     */
    private $entityManager;

    /**
     *
     * @var NativeLoader
     */
    private $nativeLoader;

    public function __construct(
        string $fixturesPath,
        EntityManager $entityManager,
        NativeLoader $nativeLoader,
        int $progressBarPercentage,
        int $progressInitialAdvantage
    ) {
        parent::__construct();
        $this->fixturesPath  = $fixturesPath;
        $this->entityManager = $entityManager;
        $this->nativeLoader  = $nativeLoader;
        $this->progressBarPercentage = $progressBarPercentage;
        $this->progressInitialAdvantage = $progressInitialAdvantage;
    }

    protected function configure()
    {
        $this
            ->setName('app:fixtures-seed')
            ->setDescription('Fakes a bunch of entities.')
            ->setHelp('This command allows you to fake a number of entities and'
                    . ' persist them');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getEntityManager();
        $loader        = $this->getNativeLoader();
        $progressBarPercentage = $this->getProgressBarPercentage();
        $progressBarInitialAdvantage = $this->getProgressInitialAdvantage();

        $io            = new SymfonyStyle($input, $output);
        $io->title('Faking');
        $io->section('Faking...');
        $io->createProgressbar($progressBarPercentage);
        $io->progressStart();
        $io->progressAdvance($progressBarInitialAdvantage);
        $objectSet     = $loader->loadFile($this->getFixturesPath());
        try {
            $generatedObjects = $objectSet->getObjects();
            $progressStep     = ($progressBarPercentage - $progressBarInitialAdvantage) / count($generatedObjects);
            foreach ($generatedObjects as $object) {
                $entityManager->persist($object);
                $entityManager->flush();
                if ($progressStep > 1) {
                    $io->progressAdvance(floor($progressStep));
                    continue;
                }
                $io->progressAdvance(1);
            }
            $io->progressFinish();
            $io->success('Data faked, fixtures seded to database');
        } catch (\Exception $e) {
            $io->progressFinish();
            $io->error($e->getMessage());
        }
    }

    public function getFixturesPath()
    {
        return $this->fixturesPath;
    }

    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    public function setFixturesPath($fixturesPath)
    {
        $this->fixturesPath = $fixturesPath;
        return $this;
    }

    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    public function getNativeLoader(): NativeLoader
    {
        return $this->nativeLoader;
    }

    public function setNativeLoader(NativeLoader $nativeLoader)
    {
        $this->nativeLoader = $nativeLoader;
        return $this;
    }
}
