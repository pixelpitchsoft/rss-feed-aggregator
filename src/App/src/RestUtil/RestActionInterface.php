<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - Nov 8, 2018
 */

namespace App\RestUtil;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
interface RestActionInterface
{
    public function display(ServerRequestInterface $request): ResponseInterface;

    public function displayList(ServerRequestInterface $request): ResponseInterface;

    public function update(ServerRequestInterface $request): ResponseInterface;

    public function delete(ServerRequestInterface $request): ResponseInterface;

    public function add(ServerRequestInterface $request): ResponseInterface;
}
