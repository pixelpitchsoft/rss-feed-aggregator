<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace Provider\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Style\SymfonyStyle;
use Provider\Entity\Provider;

/**
 * Description of LoadDataModel
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class LoadDataModel
{
    /**
     *
     * @var EntityManager
     */
    private $entityManager;

    /**
     *
     * @var string
     */
    private $dataPath;

    /**
     *
     * @var SymfonyStyle
     */
    private $io;

    public function __construct(EntityManager $entityManager, $dataPath)
    {
        $this->entityManager = $entityManager;
        $this->dataPath      = $dataPath;
    }

    public function loadData(int $progressBarPercentage)
    {
        $dataPath = $this->getDataPath();
        $entityManager = $this->getEntityManager();
        $io = $this->getIo();

        $csv = array_map('str_getcsv', file($dataPath));
        $dataPointsSum = count($csv);
        if ($dataPointsSum == 0) {
            $io->progressFinish();
            return;
        }
        $io->progressStart($dataPointsSum);
        $progressStep    = $progressBarPercentage / $dataPointsSum;
        $progressStepSum = 0;
        foreach ($csv as $row) {
            $providerRepository = $entityManager->getRepository(Provider::class);
            $provider = $providerRepository->findOneBy([
                    'name' => $row[0],
                    'url' => $row[1],
                ]);
            if ($provider instanceof Provider) {
                continue;
            }
            $provider = new Provider();
            $provider->setName($row[0]);
            $provider->setUrl($row[1]);
            $entityManager->persist($provider);
            $entityManager->flush();
            $progressStepSum += $progressStep;
            if ($progressStepSum > 1) {
                $io->progressAdvance();
                $progressStepSum = 0;
            }
        }
    }

    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    public function getDataPath()
    {
        return $this->dataPath;
    }

    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    public function setDataPath($dataPath)
    {
        $this->dataPath = $dataPath;
        return $this;
    }

    public function getIo(): SymfonyStyle
    {
        return $this->io;
    }

    public function setIo(SymfonyStyle $io)
    {
        $this->io = $io;
        return $this;
    }
}
