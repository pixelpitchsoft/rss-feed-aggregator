<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace Provider\Factory;

use Psr\Container\ContainerInterface;
use Provider\Model\LoadDataModel;

/**
 * Description of LoadDataModelFactory
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class LoadDataModelFactory
{
    /**
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     *
     * @var LoadDataModel
     */
    private $loadDataModel;

    public function __invoke(ContainerInterface $container)
    {
        $this->setContainer($container);
        $this->setUpLoadDataModel();

        return $this->getLoadDataModel();
    }

    private function setUpLoadDataModel()
    {
        $container = $this->getContainer();
        $loadDataModel = new LoadDataModel(
            $container->get('doctrine.entity_manager.orm_default'),
            $container->get('config')['rss.feed.providers.update.path']
        );

        $this->setLoadDataModel($loadDataModel);
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function getLoadDataModel(): LoadDataModel
    {
        return $this->loadDataModel;
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }

    public function setLoadDataModel(LoadDataModel $loadDataModel)
    {
        $this->loadDataModel = $loadDataModel;
        return $this;
    }
}
