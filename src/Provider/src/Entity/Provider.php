<?php

namespace Provider\Entity;

/**
 * Provider
 */
class Provider
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var \DateTime
     */
    private $createdat;

    public function __toString()
    {
        return $this->getId()->toString();
    }

        /**
     * @var \DateTime|null
     */
    private $updatedat;

    public function setCreatedatOnPrePersist()
    {
        $this->setCreatedat(new \DateTime());
    }

    public function setUpdatedatOnPreUpdate()
    {
        $this->setUpdatedat(new \DateTime());
    }


    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Provider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return Provider
     */
    public function setUrl($url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set createdat.
     *
     * @param \DateTime $createdat
     *
     * @return Provider
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat.
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set updatedat.
     *
     * @param \DateTime|null $updatedat
     *
     * @return Provider
     */
    public function setUpdatedat($updatedat = null)
    {
        $this->updatedat = $updatedat;

        return $this;
    }

    /**
     * Get updatedat.
     *
     * @return \DateTime|null
     */
    public function getUpdatedat()
    {
        return $this->updatedat;
    }
}
