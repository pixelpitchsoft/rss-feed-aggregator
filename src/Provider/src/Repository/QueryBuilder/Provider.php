<?php

namespace Provider\Repository\QueryBuilder;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Provider\Entity\Provider as ProviderEntity;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Description of DataPoint
 *
 * @author paul
 */
abstract class Provider extends EntityRepository
{
    /**
     *
     * @var QueryBuilder
     */
    protected $qb;

    public function __construct(EntityManager $entityManager)
    {
        $metadata = new ClassMetadata(ProviderEntity::class);
        parent::__construct($entityManager, $metadata);

        $this->qb = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('p.id')
            ->addSelect('p.name')
            ->addSelect('p.url')
            ->addSelect('p.createdat')
            ->addSelect('p.updatedat')
            ->from(ProviderEntity::class, 'p');
    }

    public function getQb()
    {
        return $this->qb;
    }

    public function setQb($qb)
    {
        $this->qb = $qb;
        return $this;
    }
}
