<?php

declare(strict_types=1);

namespace Provider\Api;

use Psr\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Symfony\Component\Serializer\Serializer;

class ProviderItemFactory
{
    public function __invoke(ContainerInterface $container) : ProviderItem
    {
        return new ProviderItem(
            $container->get(TemplateRendererInterface::class),
            $container->get('doctrine.entity_manager.orm_default'),
            $container->get(Serializer::class),
            $container->get('config')['api.added.headers'],
            $container->get('config')['api.format'],
            $container->get('config')['api.allowed.methods'],
            $container->get('config')['api.items.per.page'],
            $container->get('config')['api.stream.directory'],
            $container->get('config')['api.stream.mode']
        );
    }
}
