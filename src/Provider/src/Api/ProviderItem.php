<?php

declare(strict_types=1);

namespace Provider\Api;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Api\AbstractApi;
use App\Api\HttpMethodBasedInvocation;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Serializer\Serializer;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;
use Provider\Entity\Provider;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;

class ProviderItem extends AbstractApi implements RequestHandlerInterface
{

    use HttpMethodBasedInvocation;

    /**
     * @var TemplateRendererInterface
     */
    private $renderer;

    public function __construct(
        TemplateRendererInterface $renderer,
        EntityManager $entityManager,
        Serializer $serializer,
        array $addedHeaders,
        string $serializationFormat,
        array $allowedMethods,
        int $itemsPerPage,
        string $streamDirectory,
        string $streamMode
    ) {
        parent::__construct(
            $entityManager,
            $serializer,
            $addedHeaders,
            $serializationFormat,
            $allowedMethods,
            $itemsPerPage,
            $streamDirectory,
            $streamMode
        );
        $this->renderer = $renderer;
    }

    public function delete(ServerRequestInterface $request): ResponseInterface
    {
        $entityManager = $this->getEntityManager();
        $providerRepository = $entityManager->getRepository(Provider::class);
        $provider = $providerRepository->find($request->getAttribute('id'));
        if ($provider === null) {
            return (new EmptyResponse())
                            ->withStatus(StatusCode::STATUS_NOT_FOUND);
        }

        $entityManager->remove($provider);
        $entityManager->flush();

        return $this->formulateResponseAddedHeaders(new EmptyResponse());
    }

    public function display(ServerRequestInterface $request): ResponseInterface
    {
        $entityManager = $this->getEntityManager();
        $serializer    = $this->getSerializer();
        $providerRepository = $entityManager->getRepository(Provider::class);

        $provider = $providerRepository->find($request->getAttribute('id'));
        $stream   = new Stream($this->getStreamDirectory(), $this->getStreamMode());
        $stream->write($serializer->serialize($provider, $this->getSerializationFormat()));

        /* @var $response Response */
        $response = new Response($stream);

        return $this->formulateResponseAddedHeaders($response);
    }

    public function displayList(ServerRequestInterface $request): ResponseInterface
    {
        $entityManager = $this->getEntityManager();
        $providerRepository = $entityManager->getRepository(Provider::class);

        $providers = $providerRepository->findByNameAndUrlOrderByTimestamp(
            $this->getLimits()[0],
            $this->getLimits()[1],
            $request->getQueryParams()
        );

        return new JsonResponse($providers);
    }

    public function add(ServerRequestInterface $request): ResponseInterface
    {
        $entityManager = $this->getEntityManager();
        $serializer = $this->getSerializer();
        $provider = new Provider();

        $provider->setName($request->getParsedBody()['name']);
        $provider->setUrl($request->getParsedBody()['url']);

        $entityManager->persist($provider);
        $entityManager->flush();

        $stream = new Stream($this->getStreamDirectory(), $this->getStreamMode());
        $stream->write($serializer->serialize($provider, $this->getSerializationFormat()));

        /* @var $response Response */
        $response = (new Response($stream))
                ->withStatus(StatusCode::STATUS_CREATED);

        return $this->formulateResponseAddedHeaders($response);
    }

    public function update(ServerRequestInterface $request): ResponseInterface
    {
        $entityManager = $this->getEntityManager();
        $serializer = $this->getSerializer();
        $providerRepository = $entityManager->getRepository(Provider::class);

        $provider = $providerRepository->find($request->getAttribute('id'));
        if ($provider === null) {
            return (new EmptyResponse())
                ->withStatus(StatusCode::STATUS_NOT_FOUND);
        }
        $provider->setName($request->getParsedBody()['name']);
        $provider->setUrl($request->getParsedBody()['url']);

        $entityManager->persist($provider);
        $entityManager->flush();

        $stream = new Stream($this->getStreamDirectory(), $this->getStreamMode());
        $stream->write($serializer->serialize($provider, $this->getSerializationFormat()));

        /* @var $response Response */
        $response = (new Response($stream))
                ->withStatus(StatusCode::STATUS_ACCEPTED);

        return $this->formulateResponseAddedHeaders($response);
    }
}
