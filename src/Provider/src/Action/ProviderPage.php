<?php

declare(strict_types=1);

namespace Provider\Action;

use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Action\ActionBasedInvocation;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use App\Action\ActionInvocationInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Helper\UrlHelper;
use Zend\Diactoros\Response\HtmlResponse;

class ProviderPage implements RequestHandlerInterface, ActionInvocationInterface
{
    use ActionBasedInvocation;

    /**
     * @var TemplateRendererInterface
     */
    private $renderer;

    /**
     *
     * @var RouterInterface
     */
    private $router;

    /**
     *
     * @var UrlHelper
     */
    private $helper;

    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $renderer,
        UrlHelper $helper
    ) {
        $this->router        = $router;
        $this->template      = $renderer;
        $this->helper        = $helper;
    }

    public function indexAction(ServerRequestInterface $request): ResponseInterface
    {
        return new RedirectResponse($this
                ->helper
                ->generate('provider', ['action' => 'list']));
    }

    public function listAction(ServerRequestInterface $request): ResponseInterface
    {
        return new HtmlResponse($this->template->render('provider::provider-page'));
    }


    public function getRenderer(): TemplateRendererInterface
    {
        return $this->renderer;
    }

    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    public function getHelper(): UrlHelper
    {
        return $this->helper;
    }

    public function setRenderer(TemplateRendererInterface $renderer)
    {
        $this->renderer = $renderer;
        return $this;
    }

    public function setRouter(RouterInterface $router)
    {
        $this->router = $router;
        return $this;
    }

    public function setHelper(UrlHelper $helper)
    {
        $this->helper = $helper;
        return $this;
    }
}
