<?php

declare(strict_types=1);

namespace Provider\Action;

use Psr\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Helper\UrlHelper;
use Zend\Expressive\Router\RouterInterface;

class ProviderPageFactory
{
    public function __invoke(ContainerInterface $container) : ProviderPage
    {
        return new ProviderPage(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(UrlHelper::class)
        );
    }
}
