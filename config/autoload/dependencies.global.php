<?php

declare(strict_types=1);

use ContainerInteropDoctrine\EntityManagerFactory;
use Symfony\Component\Serializer\Serializer;
use App\Factory\Serializer\SerializerFactory;
use App\Command\FixtureSeedCommand;
use App\Factory\Command\FixtureSeedCommandFactory;
use App\Command\RssFeedProvidersUpdateCommand;
use App\Factory\Command\RssFeedProvidersUpdateCommandFactory;
use Provider\Model\LoadDataModel;
use Provider\Factory\LoadDataModelFactory;

return [
    // Provides application-wide services.
    // We recommend using fully-qualified class names whenever possible as
    // service names.
    'dependencies' => [
        // Use 'aliases' to alias a service name to another service. The
        // key is the alias name, the value is the service to which it points.
        'aliases' => [
            // Fully\Qualified\ClassOrInterfaceName::class => Fully\Qualified\ClassName::class,
        ],
        // Use 'invokables' for constructor-less services, or services that do
        // not require arguments to the constructor. Map a service name to the
        // class name.
        'invokables' => [
            // Fully\Qualified\InterfaceName::class => Fully\Qualified\ClassName::class,
        ],
        // Use 'factories' for services provided by callbacks/factory classes.
        'factories'  => [
            'doctrine.entity_manager.orm_default' => EntityManagerFactory::class,
            Serializer::class => SerializerFactory::class,
            FixtureSeedCommand::class => FixtureSeedCommandFactory::class,
            RssFeedProvidersUpdateCommand::class => RssFeedProvidersUpdateCommandFactory::class,
            \FastRoute\RouteCollector::class               => \App\Container\FastRouteCollectorFactory::class,
            \FastRoute\DispatcherFactory::class            => \App\Container\FastRouteDispatcherFactory::class,
            \Zend\Expressive\Router\RouterInterface::class => \App\Container\RouterFactory::class,
            LoadDataModel::class => LoadDataModelFactory::class
        ],
    ],
];
