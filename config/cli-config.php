<?php

/* 
 * Copyright (c) pixelpitchteam.yolasite.com 2018 - 2018 
 */

use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
require_once __DIR__ . '/../bootstrap/bootstrap.php';

return ConsoleRunner::createHelperSet($entityManager);
        