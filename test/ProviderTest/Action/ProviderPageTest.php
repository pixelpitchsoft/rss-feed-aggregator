<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace ProvderTest\Action;

use AppTest\ActionUtil\ActionTestCase;
use Provider\Action\ProviderPage;
use Provider\Entity\Provider;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Zend\Diactoros\Stream;
use Zend\Diactoros\Response\HtmlResponse;

/**
 * Description of ProviderPageTest
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class ProviderPageTest extends ActionTestCase
{

    public function testIndexAction()
    {
        $providerPage = $this->getMockBuilder(ProviderPage::class)
                ->disableOriginalConstructor()
                ->setMethods(['indexAction'])
                ->getMock();

        $providers = $this->compoundMultitudeCase(Provider::class);

        $serializer = $this->getSerializer();
        $stream = new Stream(self::STREAM_DIRECTORY, self::STREAM_MODE);
        $stream->write('<pre>'
                . $serializer->serialize($providers, self::SERIALIZATION_FORMAT)
                . '</pre>');

        /* @var $response Response */
        $response = new HtmlResponse($stream);
        $response = $response->withStatus(StatusCode::STATUS_OK);
        $response = $response->withAddedHeader(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE);
        $response = $response->withAddedHeader(self::ACCEPT_HEADER, self::ACCEPT_TYPE);
        $response->getBody()->rewind();

        $providerPage
                ->expects($this->once())
                ->method('indexAction')
                ->willReturn($response);

        /* @var $responseProbe Response */
        $pageResponse = $providerPage->indexAction(
            $this
                    ->prophesize(ServerRequestInterface::class)
                    ->reveal()
        );
        $pageResponse->getBody()->rewind();
        $this->assertInstanceOf(ResponseInterface::class, $pageResponse);
        $this->assertJsonStringEqualsJsonString(
            $serializer->serialize($providers, self::SERIALIZATION_FORMAT),
            strip_tags($pageResponse->getBody()->getContents())
        );
        $this->assertSame(StatusCode::STATUS_OK, $pageResponse->getStatusCode());
        $this->assertSame(self::CONTENT_TYPE, $pageResponse->getHeader(self::CONTENT_TYPE_HEADER)[0]);
        $this->assertSame(self::ACCEPT_TYPE, $pageResponse->getHeader(self::ACCEPT_HEADER)[0]);
    }
}
