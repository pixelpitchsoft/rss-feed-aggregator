<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace ProviderTest\Api;

use AppTest\RestUtil\RestTestCase;
use Provider\Api\ProviderItem;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Provider\Entity\Provider;
use Zend\Diactoros\Stream;
use Zend\Diactoros\Response\EmptyResponse;

/**
 * Description of ProviderItemTest
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class ProviderItemTest extends RestTestCase
{
    public function testDelete()
    {
        $providerItem = $this->getMockBuilder(ProviderItem::class)
                ->disableOriginalConstructor()
                ->setMethods(['delete'])
                ->getMock();

        /* @var $response Response */
        $response = (new EmptyResponse())
                ->withAddedHeader(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE)
                ->withAddedHeader(self::ACCEPT_HEADER, self::ACCEPT_TYPE);

        $providerItem
                ->expects($this->once())
                ->method('delete')
                ->willReturn($response);

        /* @var $itemResponse Response */
        $itemResponse = $providerItem->delete(
            $this
                    ->prophesize(ServerRequestInterface::class)
                    ->reveal()
        );

        $this->assertInstanceOf(ResponseInterface::class, $itemResponse);
        $this->assertEmpty($itemResponse->getBody()->getContents());
        $this->assertSame(StatusCode::STATUS_NO_CONTENT, $itemResponse->getStatusCode());
        $this->assertSame(self::CONTENT_TYPE, $itemResponse->getHeader(self::CONTENT_TYPE_HEADER)[0]);
        $this->assertSame(self::ACCEPT_TYPE, $itemResponse->getHeader(self::ACCEPT_HEADER)[0]);
    }

    public function testDisplayList()
    {
        $providerItem = $this->getMockBuilder(ProviderItem::class)
                ->disableOriginalConstructor()
                ->setMethods(['displayList'])
                ->getMock();

        $providers = $this->compoundMultitudeCase(Provider::class);

        $serializer = $this->getSerializer();
        $stream = new Stream(self::STREAM_DIRECTORY, self::STREAM_MODE);
        $stream->write($serializer->serialize($providers, self::SERIALIZATION_FORMAT));

        /* @var $response Response */
        $response = (new Response($stream))
                        ->withStatus(StatusCode::STATUS_OK)
                        ->withAddedHeader(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE)
                        ->withAddedHeader(self::ACCEPT_HEADER, self::ACCEPT_TYPE);
        $response->getBody()->rewind();

        $providerItem
                ->expects($this->once())
                ->method('displayList')
                ->willReturn($response);

        /* @var $itemResponse Response */
        $itemResponse = $providerItem->displayList(
            $this
                    ->prophesize(ServerRequestInterface::class)
                    ->reveal()
        );
        $itemResponse->getBody()->rewind();
        $this->assertInstanceOf(ResponseInterface::class, $itemResponse);
        $this->assertJsonStringEqualsJsonString(
            $serializer->serialize($providers, self::SERIALIZATION_FORMAT),
            $itemResponse->getBody()->getContents()
        );
        $this->assertSame(StatusCode::STATUS_OK, $itemResponse->getStatusCode());
        $this->assertSame(self::CONTENT_TYPE, $itemResponse->getHeader(self::CONTENT_TYPE_HEADER)[0]);
        $this->assertSame(self::ACCEPT_TYPE, $itemResponse->getHeader(self::ACCEPT_HEADER)[0]);
    }

    public function testInsert()
    {
        $providerItem = $this->getMockBuilder(ProviderItem::class)
                ->disableOriginalConstructor()
                ->setMethods(['insert'])
                ->getMock();

        $provider = new Provider();

        $serializer = $this->getSerializer();
        $stream = new Stream(self::STREAM_DIRECTORY, self::STREAM_MODE);
        $stream->write($serializer->serialize($provider, self::SERIALIZATION_FORMAT));

        /* @var $response Response */
        $response = (new Response($stream))
                        ->withStatus(StatusCode::STATUS_CREATED)
                        ->withAddedHeader(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE)
                        ->withAddedHeader(self::ACCEPT_HEADER, self::ACCEPT_TYPE);

        $providerItem
                ->expects($this->once())
                ->method('insert')
                ->willReturn($response);

        /* @var $itemResponse Response */
        $itemResponse = $providerItem->insert(
            $this
                    ->prophesize(ServerRequestInterface::class)
                    ->reveal()
        );
        $itemResponse->getBody()->rewind();

        $this->assertInstanceOf(ResponseInterface::class, $itemResponse);
        $this->assertJsonStringEqualsJsonString(
            $serializer->serialize($provider, self::SERIALIZATION_FORMAT),
            $itemResponse->getBody()->getContents()
        );
        $this->assertSame(StatusCode::STATUS_CREATED, $itemResponse->getStatusCode());
        $this->assertSame(self::CONTENT_TYPE, $itemResponse->getHeader(self::CONTENT_TYPE_HEADER)[0]);
        $this->assertSame(self::ACCEPT_TYPE, $itemResponse->getHeader(self::ACCEPT_HEADER)[0]);
    }

    public function testShow()
    {
        $providerItem = $this->getMockBuilder(ProviderItem::class)
                ->disableOriginalConstructor()
                ->setMethods(['show'])
                ->getMock();

        $provider = new Provider();

        $serializer = $this->getSerializer();
        $stream = new Stream(self::STREAM_DIRECTORY, self::STREAM_MODE);
        $stream->write($serializer->serialize($provider, self::SERIALIZATION_FORMAT));

        /* @var $response Response */
        $response = (new Response($stream))
                        ->withStatus(StatusCode::STATUS_OK)
                        ->withAddedHeader(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE)
                        ->withAddedHeader(self::ACCEPT_HEADER, self::ACCEPT_TYPE);

        $providerItem
                ->expects($this->once())
                ->method('show')
                ->willReturn($response);

        /* @var $itemResponse Response */
        $itemResponse = $providerItem->show(
            $this
                    ->prophesize(ServerRequestInterface::class)
                    ->reveal()
        );
        $itemResponse->getBody()->rewind();

        $this->assertInstanceOf(ResponseInterface::class, $itemResponse);
        $this->assertJsonStringEqualsJsonString(
            $serializer->serialize($provider, self::SERIALIZATION_FORMAT),
            $itemResponse->getBody()->getContents()
        );
        $this->assertSame(StatusCode::STATUS_OK, $itemResponse->getStatusCode());
        $this->assertSame(self::CONTENT_TYPE, $itemResponse->getHeader(self::CONTENT_TYPE_HEADER)[0]);
        $this->assertSame(self::ACCEPT_TYPE, $itemResponse->getHeader(self::ACCEPT_HEADER)[0]);
    }

    public function testUpdate()
    {
        $providerItem = $this->getMockBuilder(ProviderItem::class)
                ->disableOriginalConstructor()
                ->setMethods(['update'])
                ->getMock();

        $provider = new Provider();

        $serializer = $this->getSerializer();
        $stream = new Stream(self::STREAM_DIRECTORY, self::STREAM_MODE);
        $stream->write($serializer->serialize($provider, self::SERIALIZATION_FORMAT));

        /* @var $response Response */
        $response = (new Response($stream))
                        ->withStatus(StatusCode::STATUS_ACCEPTED)
                        ->withAddedHeader(self::CONTENT_TYPE_HEADER, self::CONTENT_TYPE)
                        ->withAddedHeader(self::ACCEPT_HEADER, self::ACCEPT_TYPE);

        $providerItem
                ->expects($this->once())
                ->method('update')
                ->willReturn($response);

        /* @var $itemResponse Response */
        $itemResponse = $providerItem->update(
            $this
                    ->prophesize(ServerRequestInterface::class)
                    ->reveal()
        );
        $itemResponse->getBody()->rewind();

        $this->assertInstanceOf(ResponseInterface::class, $itemResponse);
        $this->assertJsonStringEqualsJsonString(
            $serializer->serialize($provider, self::SERIALIZATION_FORMAT),
            $itemResponse->getBody()->getContents()
        );
        $this->assertSame(StatusCode::STATUS_ACCEPTED, $itemResponse->getStatusCode());
        $this->assertSame(self::CONTENT_TYPE, $itemResponse->getHeader(self::CONTENT_TYPE_HEADER)[0]);
        $this->assertSame(self::ACCEPT_TYPE, $itemResponse->getHeader(self::ACCEPT_HEADER)[0]);
    }
}
