<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace AppTest\RestUtil;

use PHPUnit\Framework\TestCase;
use AppTest\RestUtil\RestTestInterface;
use Zend\Serializer\Serializer;
use Zend\Serializer\Adapter\Json;
use Zend\Serializer\Adapter\PhpSerialize;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Description of RestTestCase
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
abstract class RestTestCase extends TestCase implements RestTestInterface
{
    /**
     * Compound case depth
     */
    const COMPOUND_MULTITUDE_CASE_SUM = 16;

    /**
     * Content type response headers
     */
    const CONTENT_TYPE_HEADER = 'Content-type';
    const CONTENT_TYPE        = 'application/' . self::SERIALIZATION_FORMAT;

    /**
     * Accept request headers
     */
    const ACCEPT_HEADER       = 'Accept';
    const ACCEPT_TYPE         = self::CONTENT_TYPE;

    /**
     * Response stream storage directory and mode
     */
    const STREAM_DIRECTORY    = 'php://temp';
    const STREAM_MODE         = 'wb+';

    /**
     * Symfony Serializer format
     */
    const SERIALIZATION_FORMAT = 'json';

    /**
     *
     * @var int
     */
    private $compoundMultitudeCaseSum;

    /**
     *
     * @var SymfonySerializer
     */
    private $serializer;

    public function setUp()
    {
        $this->setUpSerializer();
        $this->setUpSymfonySerializer();
        $this->setupCompoundCase();
    }

    public function tearDown()
    {
        $this->defaultSerializer();
        $this->defaultCompoundCase();
    }

    private function setUpSerializer()
    {
        Serializer::setDefaultAdapter(
            Json::class,
            [
                    'cycle_check' => true,
            ]
        );
    }

    private function setUpSymfonySerializer()
    {
        $encoders    = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new SymfonySerializer($normalizers, $encoders);

        $this->setSerializer($serializer);
    }

    private function defaultSerializer()
    {
        Serializer::setDefaultAdapter(PhpSerialize::class);
    }


    private function setupCompoundCase()
    {
        $this->setCompoundMultitudeCaseSum(self::COMPOUND_MULTITUDE_CASE_SUM);
    }

    private function defaultCompoundCase()
    {
        $this->setCompoundMultitudeCaseSum(0);
    }

    abstract public function testDelete();

    abstract public function testShow();

    abstract public function testDisplayList();

    abstract public function testInsert();

    abstract public function testUpdate();

    public function getCompoundMultitudeCaseSum()
    {
        return $this->compoundMultitudeCaseSum;
    }

    public function getSerializer(): SymfonySerializer
    {
        return $this->serializer;
    }

    public function setCompoundMultitudeCaseSum($compoundMultitudeCaseSum)
    {
        $this->compoundMultitudeCaseSum = $compoundMultitudeCaseSum;
    }

    public function setSerializer(SymfonySerializer $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     *
     * @param string $entity
     * @return array
     */
    protected function compoundMultitudeCase(string $entity): array
    {
        $compound = [];
        for ($i = 0; $i < $this->getCompoundMultitudeCaseSum(); $i++) {
            $compound []= new $entity();
        }

        return $compound;
    }
}
