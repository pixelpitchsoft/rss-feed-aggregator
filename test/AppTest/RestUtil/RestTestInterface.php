<?php

/*
 * Copyright (c) pixelpitchteam.yolasite.com 2017 - 2018
 */

namespace AppTest\RestUtil;

/**
 *
 * @author paul
 */
interface RestTestInterface
{
    public function testDelete();

    public function testShow();

    public function testDisplayList();

    public function testInsert();

    public function testUpdate();
}
