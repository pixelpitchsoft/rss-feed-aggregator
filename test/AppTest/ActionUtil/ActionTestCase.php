<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace AppTest\ActionUtil;

use PHPUnit\Framework\TestCase;
use AppTest\ActionUtil\ActionTestInterface;
use Zend\Serializer\Serializer;
use Zend\Serializer\Adapter\Json;
use Zend\Serializer\Adapter\PhpSerialize;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

/**
 * Description of ActionTestCase
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
abstract class ActionTestCase extends TestCase implements ActionTestInterface
{
    /**
     * Compound case depth
     */
    const COMPOUND_MULTITUDE_CASE_SUM = 16;

    /**
     * Content type response headers
     */
    const CONTENT_TYPE_HEADER = 'Content-type';
    const CONTENT_TYPE        = 'text/html; charset=utf-8';

    /**
     * Accept request headers
     */
    const ACCEPT_HEADER       = 'Accept';
    const ACCEPT_TYPE         = 'application/x-www-form-urlencoded';

    /**
     * Response stream storage directory and mode
     */
    const STREAM_DIRECTORY    = 'php://temp';
    const STREAM_MODE         = 'wb+';

    /**
     * Symfony Serializer format
     */
    const SERIALIZATION_FORMAT = 'json';

    /**
     *
     * @var SymfonySerializer
     */
    private $serializer;

    /**
     *
     * @var int
     */
    private $compoundMultitudeCaseSum;

    public function setUp()
    {
        $this->setUpSerializer();
        $this->setUpSymfonySerializer();
        $this->setupCompoundCase();
    }

    public function tearDown()
    {
        $this->defaultSerializer();
        $this->defaultCompoundCase();
    }

    private function setUpSerializer()
    {
        Serializer::setDefaultAdapter(
            Json::class,
            [
                    'cycle_check' => true,
                ]
        );
    }

    private function setUpSymfonySerializer()
    {
        $encoders    = [new JsonEncoder()];
        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(),
        ];

        $serializer = new SymfonySerializer($normalizers, $encoders);

        $this->setSerializer($serializer);
    }

    /**
     *
     * @param string $entity
     * @return array
     */
    protected function compoundMultitudeCase(string $entity): array
    {
        $compound = [];
        for ($i = 0; $i < $this->getCompoundMultitudeCaseSum(); $i++) {
            $compound []= new $entity();
        }

        return $compound;
    }

    private function defaultSerializer()
    {
        Serializer::setDefaultAdapter(PhpSerialize::class);
    }

    public function getSerializer(): SymfonySerializer
    {
        return $this->serializer;
    }

    public function setSerializer(SymfonySerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function getCompoundMultitudeCaseSum()
    {
        return $this->compoundMultitudeCaseSum;
    }

    public function setCompoundMultitudeCaseSum($compoundMultitudeCaseSum)
    {
        $this->compoundMultitudeCaseSum = $compoundMultitudeCaseSum;
        return $this;
    }

    private function setupCompoundCase()
    {
        $this->setCompoundMultitudeCaseSum(self::COMPOUND_MULTITUDE_CASE_SUM);
    }

    private function defaultCompoundCase()
    {
        $this->setCompoundMultitudeCaseSum(0);
    }

    abstract public function testIndexAction();
}
