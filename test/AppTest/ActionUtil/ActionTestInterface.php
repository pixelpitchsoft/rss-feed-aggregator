<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace AppTest\ActionUtil;

/**
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
interface ActionTestInterface
{
    public function testIndexAction();
}
