/* 
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

import _ from 'underscore';
import $ from 'jquery';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/custom.css';
import Backbone from 'backbone';

import './index.js';

console.log('dom.ready');
console.log($('title').text());
console.log('page loaded, app working');

console.log(window.template);

window.providerApi = $('data#api').data('apiProvider');
console.log($('[data-api-provider!=""]').length);
console.log($('data[data-api-provider!=""]').length);
console.log($('data#api'));
console.log($('data#api').data('apiProvider'));
console.log(providerApi);
window.App.Models.Provider = Backbone.Model.extend({
    urlRoot: providerApi
});

console.log(App.Models.Provider.url);

window.App.Collections.Providers = Backbone.Collection.extend({
    model: App.Models.Provider,
    url: providerApi
});

window.App.Views.Provider = Backbone.View.extend({
    tagName: 'tr',
    template: template('providerTemplate'),
    render: function ()
    {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    events: {
        'click .provider-name': 'display'
    },
    display: function() {
        $('.modal').find('.feed-name').html(this.model.toJSON()['name']);
        var model = this.model;
        $('.modal').find('.btn-primary')
            .unbind('click')
            .on('click', function(){
                window.open(model.toJSON()['url'], '_blank');
            });
    }
});
window.App.Views.Providers = Backbone.View.extend({
    initialize: function () {
        this.collection.fetch();
        this.listenTo(this.collection, 'reset change remove',
                this.render, this);
        this.collection.on('add', this.addOne, this);
    },
    tagName: 'tbody',
    render: function () {
        this.$el.html('');
        this.collection.each(this.addOne, this);
        console.log('el log');
        console.log(this.$el);
        console.log(this.$el.closest('table').length);
        console.log('tbody');
        console.log(this.$el);
        return this;
    },
    addOne: function (model) {
        console.log('model test');
        console.log(model);
        var view = new App.Views.Provider({model: model});
        this.$el.append(view.render().el);
    }
});

var providersCollection = new window.App.Collections.Providers();
console.log(providersCollection.url);

var providersView = new App.Views.Providers({
        collection: providersCollection
    });
console.log('providersView.collection');
console.log(providersView.collection);
$('#providerGrid').append(providersView.render().el);

