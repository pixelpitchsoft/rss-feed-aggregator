/* 
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */
import _ from 'lodash';
import $ from 'jquery';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import backbone from 'backbone';

import './../css/style.css';

console.log('index.loaded');
console.log($('title').text());

window.template = function (id) {
    return _.template($('#' + id)
            .html());
};

_.template.formatDateYear = function (stamp) {
    var date = new Date(stamp);
    return date.getFullYear();
};


window.App = {
    Models: {},
    Collections: {},
    Views: {}
};