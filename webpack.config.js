/* 
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: {
        app: './assets/js/index.js',
        provider: './assets/js/provider.js'
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './public/dist',
        hot: true
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['public/dist']),
        new HtmlWebpackPlugin({
            title: 'Output Management'
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    output: {
      filename: '[name].core.js',
//      chunkFilename: '[name].core.js',
      path: path.resolve(__dirname, 'public/dist'),
      publicPath: '/'
    },
//    optimization: {
//        splitChunks: {
//            chunks: 'all'
//        }
//    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    }
};
