#!/usr/bin/env php
<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

require __DIR__.'/../vendor/autoload.php';
require __DIR__ . '/../bootstrap/bootstrap.php';

use Symfony\Component\Console\Application;
use App\Command\FixtureSeedCommand;
use App\Command\RssFeedProvidersUpdateCommand;

$application = new Application();

$application->add($container->get(FixtureSeedCommand::class));
$application->add($container->get(RssFeedProvidersUpdateCommand::class));
$application->setDefaultCommand($container->get(FixtureSeedCommand::class)->getName(), false);

$application->run();
