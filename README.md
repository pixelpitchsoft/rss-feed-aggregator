# Installation

```
composer install
npm install
npm run build
```
Set database credentials in [config/autoload/doctrine.local.php].

Actually create MySQL user and grant access to a created database that you specified
in credentials configuration file.

Create database schema:
```
vendor/bin/doctrine orm:schema-tool:create
```

Validate it:
```
vendor/bin/doctrine orm:validate-schema
```

If not validating (usually second phase due to Doctrine limitations), run
```
vendor/bin/doctrine orm:schema-tool:update --force
```

Manually execute cronjob, which will read [feed/providers.csv] to the database
```
bin/application.php app:rss-providers-update
```

To generate some (readable) random data you can also execute command
```
bin/application.php app:fixtures-seed
```

Configure Apache2 virtual host (see appendix for an example)

Unit cases:
```
cp phpunit.xml.dist phpunit.xml
composer test
```

Unit cases with coverage (see that Entity is covered, probing for virtual
application responses from business side).
```
phpunit --coverage-html=cov
```

# Possible improvements

* Filtering by JS queries to API the callback JSON, as due to the limitations
the task has been submitted ASAP
* Database improvement by extension, cron job crawling the latest news feed
article/post and adding it to the database RSS feed provider row

# N. B.

Some RSS news feed providers do not link to the actual working pages, they
were copied from internet recommending blog articles.

All submitted repository code has been checked by PHPCS (Code Sniffer) and
PHPMD (Mess Detector) by git hooks (precommit) to eliminate errors and to adhere
to Zend2 standards. It by default has to readable and convertible.

The Zend Expressive framework follows PSR series standards, it is specifically
adapted/developed to meet their requirements.

Queues and deques are missing by default in PHP7 - latest, but available as PECL
extension, which requires GCC compiler and at the development its SUSE server
was unavailable - i. e. it was uninstallable.

# Appendix

Vhost configuration:

```apacheconf
#
# VirtualHost template
# Note: to use the template, rename it to /etc/apache2/vhost.d/yourvhost.conf. 
# Files must have the .conf suffix to be loaded.
#
# See /usr/share/doc/packages/apache2/README.QUICKSTART for further hints 
# about virtual hosts.
#
# Almost any Apache directive may go into a VirtualHost container.
# The first VirtualHost section is used for requests without a known
# server name.
#
<VirtualHost *:80>
    ServerAdmin povilas@localhost
    ServerName localhost.presentconnection

    # DocumentRoot: The directory out of which you will serve your
    # documents. By default, all requests are taken from this directory, but
    # symbolic links and aliases may be used to point to other locations.
    DocumentRoot /srv/www/vhosts/presentconnection-rss-reader-aggregator-zf-expr/public

    # if not specified, the global error log is used
    ErrorLog /var/log/apache2/presentconnection-rss-reader-aggregator-zf-expr-error_log
    CustomLog /var/log/apache2/presentconnection-rss-reader-aggregator-zf-expr-access_log combined

    # don't loose time with IP address lookups
    HostnameLookups Off

    # needed for named virtual hosts
    UseCanonicalName Off

    # configures the footer on server-generated documents
    ServerSignature On


    # Optionally, include *.conf files from /etc/apache2/conf.d/
    #
    # For example, to allow execution of PHP scripts:
    #
    # Include /etc/apache2/conf.d/php5.conf
    #
    # or, to include all configuration snippets added by packages:
    # Include /etc/apache2/conf.d/*.conf


    # ScriptAlias: This controls which directories contain server scripts.
    # ScriptAliases are essentially the same as Aliases, except that
    # documents in the realname directory are treated as applications and
    # run by the server when requested rather than as documents sent to the client.
    # The same rules about trailing "/" apply to ScriptAlias directives as to
    # Alias.
    #
    ScriptAlias /cgi-bin/ "/srv/www/vhosts/presentconnection-rss-reader-aggregator-zf-expr/cgi-bin/"

    # "/srv/www/cgi-bin" should be changed to whatever your ScriptAliased
    # CGI directory exists, if you have one, and where ScriptAlias points to.
    #
    <Directory "/srv/www/vhosts/presentconnection-rss-reader-aggregator-zf-expr/cgi-bin">
        AllowOverride None
        Options +ExecCGI -Includes
        <IfModule !mod_access_compat.c>
            Require all granted
        </IfModule>
        <IfModule mod_access_compat.c>
            Order allow,deny
            Allow from all
        </IfModule>
    </Directory>


    # UserDir: The name of the directory that is appended onto a user's home
    # directory if a ~user request is received.
    #
    # To disable it, simply remove userdir from the list of modules in APACHE_MODULES
    # in /etc/sysconfig/apache2.
    #
    <IfModule mod_userdir.c>
        # Note that the name of the user directory ("public_html") cannot simply be
        # changed here, since it is a compile time setting. The apache package
        # would have to be rebuilt. You could work around by deleting
        # /usr/sbin/suexec, but then all scripts from the directories would be
        # executed with the UID of the webserver.
        UserDir public_html
        # The actual configuration of the directory is in
        # /etc/apache2/mod_userdir.conf.
        Include /etc/apache2/mod_userdir.conf
        # You can, however, change the ~ if you find it awkward, by mapping e.g.
        # http://www.example.com/users/karl-heinz/ --> /home/karl-heinz/public_html/
        #AliasMatch ^/users/([a-zA-Z0-9-_.]*)/?(.*) /home/$1/public_html/$2
    </IfModule>


    #
    # This should be changed to whatever you set DocumentRoot to.
    #
    <Directory "/srv/www/vhosts/presentconnection-rss-reader-aggregator-zf-expr/public">

        #
        # Possible values for the Options directive are "None", "All",
        # or any combination of:
        #   Indexes Includes FollowSymLinks SymLinksifOwnerMatch ExecCGI MultiViews
        #
        # Note that "MultiViews" must be named *explicitly* --- "Options All"
        # doesn't give it to you.
        #
        # The Options directive is both complicated and important.  Please see
        # http://httpd.apache.org/docs/2.4/mod/core.html#options
        # for more information.
        #
        Options Indexes FollowSymLinks

        #
        # AllowOverride controls what directives may be placed in .htaccess files.
        # It can be "All", "None", or any combination of the keywords:
        #   Options FileInfo AuthConfig Limit
        #
        AllowOverride FileInfo

        #
        # Controls who can get stuff from this server.
        #
        <IfModule !mod_access_compat.c>
            Require all granted
        </IfModule>
        <IfModule mod_access_compat.c>
            Order allow,deny
            Allow from all
        </IfModule>

    </Directory>

</VirtualHost>
```
